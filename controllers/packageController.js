const Package = require('../models/Package');
const catchAsync = require('../utils/catchAsync');
const factory = require('../controllers/handlerFactory');


exports.getAll = factory.getAll(Package);
exports.getOne = factory.getOne(Package);
exports.create = factory.createOne(Package);
exports.updateOne = factory.updateOne(Package);
exports.deleteOne = factory.deleteOne(Package);