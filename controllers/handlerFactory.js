const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');



exports.deleteOne = Model =>
  catchAsync(async (req, res, next) => {
    Model.destroy({
      where: {
        [Model.primaryKeyAttributes]: req.params.id
      }
    });

    res.status(204).json({
      status: 'success',
      data: null
    });
  });

exports.updateOne = Model =>
  catchAsync(async (req, res, next) => {
    await Model.update(req.body, {
      where: { [Model.primaryKeyAttributes]: req.params.id }
    });

    const result = await Model.findByPk(req.params.id);

    res.status(201).json({
      result
    });
  });

exports.createOne = Model =>
  catchAsync(async (req, res, next) => {
    const result = await Model.create(req.body);

    if (!result) return next(new AppError(404, 'Record was not created.'));

    res.status(201).json({
      result
    });
  });

exports.getOne = (Model, includes) =>
  catchAsync(async (req, res, next) => {
    const result = await Model.findByPk(req.params.id, includes);

    if (!result) return next(new AppError(404, 'Result was not found.'));

    res.status(200).json({
      result
    });
  });

exports.getAll = (Model, includes) =>
  catchAsync(async (req, res, next) => {

    let result = await Model.findAll(includes);
    if (!result) return next(new AppError(404, 'No results found.'));
    switch (Model.getTableName()) {
      case 'services':
          result = await Model.findAll(
            {
              where: {
                [Op.and]: [
                  req.query.start_location !== 'undefined' ? { start_destination: req.query.start_location } : null,
                  req.query.end_location !== 'undefined'? { end_destination: req.query.end_location } : null,
                  req.query.date ? { date: req.query.date } : null 
                ]
              },
              ...includes
            }
          )
        break;
    }

    res.status(200).json({
      result,

    });
  });

exports.getAllByColumn = (
  Model,
  includes,
  id,
  columnName,
  additionalColumnName = null
) =>
  catchAsync(async (req, res, next) => {
    const columnValue = req.params[id];

    let result = await Model.findAll({
      where: {
        [columnName]: {
          [Op.eq]: columnValue
        }
      },
      ...includes
    });

    if (!result) return next(new AppError(404, 'No results found.'));

    if (additionalColumnName) {
      result = result.map(el => {
        return el[additionalColumnName];
      });
    }

    res.status(200).json({
      result
    });
  });
