const factory = require('./handlerFactory');
const Sequelize = require('sequelize');
const UserToService = require('../models/UserToService');
const Service = require('../models/Service');
const RequestStatus = require('../models/RequestStatus');
const User = require('../models/User');
const catchAsync = require('../utils/catchAsync');
const Location = require('../models/Location')
const Op = Sequelize.Op;
//export pdf packages
const puppeteer = require('puppeteer');
const fs = require('fs-extra');

const includes = {
    include: [
        {
            model: Service,
            as: 'service',
            include: [
                {
                    model: Location,
                    as: 'start_des'
                },
                {
                    model: Location,
                    as: 'end_des'
                },
                {
                    model: User
                },

            ],
        },
        {
            model: User,
            as: 'user'
        },
        {
            model: RequestStatus,
            as: 'requestStatus'
        }

    ],
    attributes: { exclude: ['service_id', 'user,', 'request_status_id'] },
};
exports.getAll = factory.getAll(UserToService, includes);
exports.getOne = factory.getOne(UserToService);
exports.create = factory.createOne(UserToService);
exports.updateOne = factory.updateOne(UserToService);
exports.deleteOne = factory.deleteOne(UserToService);

exports.usersAssignedToService = catchAsync(async (req, res, next) => {
    const serviceId = req.params.serviceId
    const requestId = Number(req.query.requestId)
    console.log(requestId)
    console.log(serviceId)
    let result = [];
    if (requestId) {
        result = await UserToService.findAll({
            where: [
                {
                    service_id: serviceId,

                }
            ],
            ...includes
        })
    }
    else result = await UserToService.findAll({ where: { service_id: serviceId }, ...includes })

    res.status(200).json({
        result: result
    })
});
exports.getUsersServices = catchAsync(async (req, res, next) => {
    const userId = Number(req.params.userId)
    const requestStatus = Number(req.params.requestStatus)
    const result = await UserToService.findAll({
        where: {
            user_id: userId, [Op.and]: [
                { request_status_id: requestStatus },

            ]
        },
        ...includes
    })
    res.status(200).json({
        result: result
    })
})
exports.updateRequestStatus = catchAsync(async (req, res, next) => {
    const serviceId = Number(req.params.serviceId)
    const userId = Number(req.params.userId)
    const requestId = Number(req.params.requestId);
    try {
        const result = await UserToService.update(
            { request_status_id: requestId },
            {
                where: {
                    service_Id: serviceId,
                    [Op.and]: [
                        { user_id: userId },
                    ]
                }
            }
        )
        console.log('updated')
    } catch (err) {
        console.log(err)
    }


    res.status(200).json({
        message: 'updated'
    })
});
function convertDate() {
    const date = new Date();

    const year = new Date().getFullYear();

    let month = date.getMonth() + 1;
    month = (month < 10) ? "0" + month.toString() : month;
    let day = (date.getDate() < 10) ? "0" + date.getDate().toString() : date.getDate();
    const finalDate = year + `-` + month + '-' + day;
    return finalDate;
}
exports.createUserServicePdf = catchAsync(async (req, res, next) => {
    const today = convertDate();
    console.log("TODAY", today)
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    let result = [];
    result = await UserToService.findAll({
        ...includes,
        where:
        {
            '$service.date$': today,
            [Op.and]: [
                { request_status_id: 1 },
            ],

        }


    })
    res.status(200).json({
        result: result
    })
    await page.setContent(`
    <h2>Voznje za danas</h2>
    ${result.map(el => {
        return `<h2>${el.service.start_des.city + `-` + el.service.end_des.city}  Vreme polazka: ${el.service.time}<h2>
        <table style="border-collapse: collapse;width: 100%;font-family: arial, sans-serif;">
        <tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;">
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Ime i prezime</th>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Broj telefon</th>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Email</th>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Adresa - ${el.service.start_des.city} </th>
        <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Adresa - ${el.service.end_des.city} </th>
      </tr>
      <tr>
         ${el.service.users.map(user => {
            return `<tr style="border: 1px solid #dddddd;text-align: left;padding: 8px;"><td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">${user.full_name}</td>
            <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">${user.phone_number}</td><td>${user.username}</td>
            <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">Test POcetna</td><td>Test krajnja</td></tr>`
        })}
      </tr>
        
        </table>

       `.replace(/,/g, '');
    })}

    
    
    
    `);
    await page.emulateMediaType('screen');
    await page.pdf({
        path: 'ftp/VožnjeZaDanas.pdf',
        format: 'A4',
        printBackground: true
    });

    console.log('done');
    await browser.close();

});