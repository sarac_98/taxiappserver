const RequestStatus = require('../models/RequestStatus');
const factory = require('../controllers/handlerFactory');




exports.getAll = factory.getAll(RequestStatus);
exports.getOne = factory.getOne(RequestStatus);
exports.create = factory.createOne(RequestStatus);
exports.updateOne = factory.updateOne(RequestStatus);
exports.deleteOne = factory.deleteOne(RequestStatus);