const Vehicle = require('../models/Vehicle');
const catchAsync = require('../utils/catchAsync');
const factory = require('../controllers/handlerFactory');


exports.getAll = factory.getAll(Vehicle);
exports.getOne = factory.getOne(Vehicle);
exports.create = factory.createOne(Vehicle);
exports.updateOne = factory.updateOne(Vehicle);
exports.deleteOne = factory.deleteOne(Vehicle);