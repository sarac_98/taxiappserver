const Location = require('../models/Location');
const catchAsync = require('../utils/catchAsync');
const factory = require('../controllers/handlerFactory');



exports.checkIfLocationExists = catchAsync(async (req, res, next) => {

    const result = await Location.findOne({
        where: { city: req.query.location }
    })
  
        res.status(200).json({
            status: 'Success.',
            exists: result ? true : false
        });
    
    //  if(req.query.location)
    console.log("result", result)

})
exports.getAll = factory.getAll(Location);
exports.getOne = factory.getOne(Location);
exports.create = factory.createOne(Location);
exports.updateOne = factory.updateOne(Location);
exports.deleteOne = factory.deleteOne(Location);