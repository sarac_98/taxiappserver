const factory = require('./handlerFactory');
const UserToServiceToPackage = require('../models/UserToServiceToPackage');
// const Service = require('../models/Service');
// const User = require('../models/User');
// const catchAsync = require('../utils/catchAsync');

// const includes = {
//   include: [
//     {
//       model: Service,
//       as: 'service',

      
//     },
//     {
//       model: User,
//       as:'user'
//     },
//   ],
//   attributes: { exclude: ['user_id', 'service_id'] },
// };
// const includesByClient = {
//   include: [
//     {
//       model: Service,
//       include: [
//         {
//           model: ServiceCategory,
//         },
//       ],
//       attributes: { exclude: ['category_id'] },
//     },
//   ],
//   attributes: { exclude: ['service_id', 'client_id'] },
// };

exports.getAll = factory.getAll(UserToServiceToPackage);
exports.getOne = factory.getOne(UserToServiceToPackage);
exports.create = factory.createOne(UserToServiceToPackage);
exports.updateOne = factory.updateOne(UserToServiceToPackage);
exports.deleteOne = factory.deleteOne(UserToServiceToPackage);