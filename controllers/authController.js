require('dotenv').config()
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');
const sendEmail = require('../utils/email');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const bcrypt = require('bcrypt');

//const jwt = require('jsonwebtoken');
require('dotenv').config({ path: "./config.env" });
const signToken = (user) => {
  return jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET);
};

const createSendToken = async (user, statusCode, res) => {
  const token = signToken(user);
  const cookieOptions = {
    httpOnly: true,
  };

  if (process.env.NODE_ENV === 'production') cookieOptions.secure = true;

  res.cookie('jwt', token, cookieOptions);

  // Remove password from output
  user.password = undefined;

  // return console.log(user);

  await user;

  res.status(statusCode).json({
    status: 'success',
    token,
    data: {
      user,
    },
  });
};

exports.login = catchAsync(async (req, res, next) => {

  const username = req.body.username;
  const password = req.body.password;
  if (!username || !password) {
    return next(new AppError('Please provide email and password!', 400));
  }
  console.log(req)

  const user = await User.findOne({
    where: {
      username: username,
    }
  })


  if (!user || !(await user.checkPasword(password)))
    return next(new AppError(404, 'Username and/or password are incorect.'));

  //const accessToken = jwt.sign(user.toJSON(),process.env.ACCESS_TOKEN_SECRET);
  //res.json({accessToken:accessToken})
  createSendToken(user.toJSON(), 200, res);
  console.log(user.toJSON())



});
exports.changePassword = catchAsync(async (req, res, next) => {
  const update = req.body;
  const userId = req.params.userId;
  console.log(userId)

  let user = await User.findOne({
    where: {
      user_id: userId,
    },
  });

  if (!user || !(await user.checkPasword(update.currentPassword))) {
    return next(
      new AppError(404, "Username donesn't exist.Or password is incorect.")
    );
  }

  if (update.password !== update.passwordRetype) {
    return next(
      new AppError(404, 'Password and retype password are not same.')
    );
  }

  const passwordUpdated = await bcrypt.hash(update.password, 12);

  await User.update(
    {
      password: passwordUpdated,
    },
    {
      where: { [User.primaryKeyAttributes]: userId },
    }
  );
  createSendToken(user, 201, res);
});

exports.signup = catchAsync(async (req, res, next) => {

  const NewUser = {
    // user_id: req.body.user_id,
    username: req.body.username,
    password: req.body.password,
    user_type: req.body.user_type,
    full_name: req.body.full_name,
    adress: req.body.adress,
    phone_number: req.body.phone_number,
    created_at: Date.now(),
    updated_at: Date.now(),
    //isWebUser: true,
  };

  const result = await User.create(NewUser);
  
  if (!result)
    return next(new AppError(404, 'Record was not added to old DB.'));
  
  res.status(201).json({
    result: NewUser,
  });
});
exports.forgotPassword = catchAsync(async (req, res, next) => {
  const email = req.body.email;

  const user = await User.findOne({
    where: {
      username: email,
    },
    attributes: { exclude: 'password' },
  });

  if (!user) return next(new AppError('Non existing email.', 404));

  const token = signToken(user);
  const cookieOptions = {
    httpOnly: true,
  };

  res.cookie('jwt', token, cookieOptions);
  // http://localhost:3000/resetPassword/

  try {
    sendEmail({
      subject: `Šarac Taxi - password reset (${Date.now()})`,
      email: email,
      html: `<div><h1>Password Reset</h1><h5>Password reset link u nastavku:</h5><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" style="border-radius: 3px;" bgcolor=" #3E51B5 "><a href="http://localhost:3000/resetPassword/${token}" target="_blank" style=" font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 12px 18px; border: 1px solid #3E51B5; display: inline-block;">Resetuj lozinku</a></td>
              </tr>
            </table>
          </td>
        </tr>
      </table></div>`,
    });
  } catch (error) {
    return next(new AppError(error, 500));
  }

  res.status(201).json({
    status: 'success',
    message:
      'Email containing reset password token was sent to users e-mail successfully.',
  });
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  const newPassword = req.body.password;
  const token = req.params.token;

  const decoded = await promisify(jwt.verify)(token, '010ba5b824c602296c0dc220828e07980b76c88af63a2ec9bf85c5f9f796355c397a64e417a0b4fbb71235d87ae08afba3e3a64be8861b3c8cc5b18aa6848e89');
  const user = decoded.user;

  console.log(user);

  if (!user || !newPassword)
    return next(new AppError('Password change unsuccessfull.'));

  const encryptedPassword = await bcrypt.hash(newPassword, 12);

  await User.update(
    {
      password: encryptedPassword,
    },
    {
      where: { [User.primaryKeyAttributes]: user.user_id },
    }
  );

  res.status(200).json({
    status: 'Success.',
    message: `Password successfully updated for user: ${user.username}`,
  });
});