const Service = require('../models/Service');
const catchAsync = require('../utils/catchAsync');
const factory = require('../controllers/handlerFactory');
const Vehicle = require('../models/Vehicle');
const Location = require('../models/Location');
const consts = require('../consts/serviceConsts');
const 

serviceIncludes = {
  include: [
    {
      model: Vehicle,
      as: consts.MODEL_VEHICLE_NAME,
      attributes: [
        consts.VEHICLE_ID,
        consts.VEHICLE_MODEL,
        consts.SEATS_NUM,
        // sectorConsts.MODEL_WORKING_UNIT
      ],
    },
    {
      model:Location,
      as: 'start_des',   
      attributes: [
        'id',
        'city'
        // sectorConsts.MODEL_WORKING_UNIT
      ],

    },
    {
      model:Location,
      as: 'end_des',   
      attributes: [
        'id',
        'city'
        // sectorConsts.MODEL_WORKING_UNIT
      ],

    },
    
 
    

   
  ],
 
}

exports.getAll = factory.getAll(Service,serviceIncludes);
exports.getOne = factory.getOne(Service);
exports.create = factory.createOne(Service);
exports.updateOne = factory.updateOne(Service);
exports.deleteOne = factory.deleteOne(Service);