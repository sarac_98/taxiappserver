const User = require('../models/User');
const catchAsync = require('../utils/catchAsync');
const Service = require('../models/Service');
const multer = require('multer');
const fs = require('fs-extra');

const factory = require('../controllers/handlerFactory');
const userConsts = require('../consts/userConsts');
const serviceConsts = require('../consts/serviceConsts');
const userIncludes = {
  include: [
    {
      model: Service,
      as: userConsts.MODEL_SERVICE_NAME,

    },
  ],
  attributes: {
    exclude: [
      userConsts.USER_FOREIGN_KEY_SERVICE

    ],
  },
};
exports.getAll = factory.getAll(User);
exports.getOne = factory.getOne(User);
exports.create = factory.createOne(User);
exports.updateOne = factory.updateOne(User);
exports.deleteOne = factory.deleteOne(User);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = 'ftp/';
    fs.ensureDirSync(dir);
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
exports.upload = multer({ storage: storage });
exports.uploadFile = catchAsync(async (req, res, next) => {

  const path = req.file.path.replace('\\', '/');
  console.log(path)
  res.json({
    status: 'success',
    path,
  });
});

exports.checkIfEmailExists = catchAsync(async (req, res, next) => {
  const email = req.query.username;
  const user = await User.findOne({
    where: {
      username: email,
    }
  });
  res.status(201).json({
    emailExists: user ? true : false
  });


});