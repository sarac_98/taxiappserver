const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('nodemailer-sendgrid');

const sendEmail = async (opt) => {
  // sgMail.setApiKey(process.env.SEND_GRID_API_KEY);
  // const msg = {
  //   to: 'nebojsa.ognjanovic@underit.rs', // Change to your recipient
  //   from: 'nebojsaog@gmail.com', // Change to your verified sender
  //   subject: 'Sending with SendGrid is Fun',
  //   text: 'and easy to do anywhere, even with Node.js',
  //   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  // };
  // sgMail
  //   .send(msg)
  //   .then(() => {
  //     console.log('Email sent');
  //   })
  //   .catch((error) => {
  //     console.error(error);
  //   });
  const transport = nodemailer.createTransport(
    nodemailerSendgrid({
      apiKey: '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'
    })
  );

  let email = {
    from: 'predrag.sarac@underit.rs',
    to: opt.email,
    subject: opt.subject,
    html: opt.html,
  };

  await transport.sendMail(email);
};

module.exports = sendEmail;
