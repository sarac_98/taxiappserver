const Sequelize = require('sequelize');
const db = require('../database/database');
const User = require('./User');
const Service = require('./Service');
const Package = require('../models/Package');
const UserToServiceToPackage = db.define(
  'map_users_to_services_to_package',
  {
    service_id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    user_id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    package_id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
      }
  },
  {
    underscored: true,
    timestamps: false,
  }
);

UserToServiceToPackage.belongsTo(Service, { foreignKey: 'service_id', as: 'service' });
UserToServiceToPackage.belongsTo(User, { foreignKey: "user_id", as: 'user' });
UserToServiceToPackage.belongsTo(Package, { foreignKey: "package_id", as: 'package' });

module.exports = UserToServiceToPackage;