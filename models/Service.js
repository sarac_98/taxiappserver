const Sequelize = require('sequelize')
const db = require('../database/database');
const consts = require('../consts/serviceConsts');
const Location = require('../models/Location');
const Vehicle = require('./Vehicle');
const User = require('./User');

const Service = db.define(
  consts.SERVICE_TABLE_NAME,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    date: {
      type: Sequelize.DATEONLY
    },
    time: {
      type: Sequelize.DATE
    },
    time_end: {
      type: Sequelize.DATE
    },
    price: {
      type: Sequelize.INTEGER
    }




  },
  {
    underscored: true,
    timestamps: false,
  }
);
Service.belongsTo(Location, { foreignKey: 'start_destination', as: 'start_des' });
Service.belongsTo(Location, { foreignKey: 'end_destination', as: 'end_des' });
Service.belongsTo(Vehicle, { foreignKey: 'vehicle_id', });


module.exports = Service;