const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const consts = require('../consts/userConsts');
const db = require('../database/database');
const Service = require('./Service');
const User = db.define(
  consts.USER_TABLE_NAME,
  {
    user_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    full_name: {
      type: Sequelize.STRING
    },
    phone_number: {
      type: Sequelize.STRING
    },
    adress: {
      type: Sequelize.STRING
    },
    username: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
    user_type: {
      type: Sequelize.INTEGER
    },
    online_status: {
      type: Sequelize.INTEGER
    },
    picture_path: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    }
  },
  {
    underscored: true,
    timestamps: true,
  }
);

//CLASS FUNCTIONS
User.beforeCreate(async (user, options) => {
  console.log(user);
  return (user.dataValues.password = await bcrypt.hash(
    user.dataValues.password,
    12
  ));
});

User.prototype.checkPasword = async function (pass) {
  const hash = await bcrypt.hash(pass, 12);
  console.log(this);
  console.log(this.password, hash);
  return await bcrypt.compare(pass, this.password);
};


module.exports = User;