const Sequelize = require('sequelize')
const db = require('../database/database');
const consts = require('../consts/requestsStatusConsts');

const RequestStatus = db.define(
    'request_status',
    {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        request_status: {
            type: Sequelize.STRING
        },
        note: {
            type: Sequelize.STRING
        }
    },
    {
        underscored: true,
        timestamps: false,
    }
);


module.exports = RequestStatus;