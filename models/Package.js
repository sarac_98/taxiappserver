const Sequelize = require('sequelize')
const db = require('../database/database');
const consts = require('../consts/packageConsts');

const Package = db.define(
  consts.PACKAGE_TABLE_NAME,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    volume: {
        type: Sequelize.DOUBLE
    }
},
  {
    underscored: true,
    timestamps: false,
  }
);


  module.exports = Package;