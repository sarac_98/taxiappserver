const Sequelize = require('sequelize')
const db = require('../database/database');
const consts = require('../consts/vehincleConsts');

const Vehicle = db.define(
    consts.VEHICLE_TABLE_NAME,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    seats_num: {
      type: Sequelize.INTEGER
    },
    model: {
        type: Sequelize.STRING
    }
   
},
  {
    underscored: true,
    timestamps: false,
  }
);


  module.exports = Vehicle;