const Sequelize = require('sequelize')
const db = require('../database/database');
const consts = require('../consts/locationConsts');

const Location = db.define(
  consts.LOCATION_TABLE_NAME,
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    city: {
      type: Sequelize.STRING
    },
  
   
},
  {
    underscored: true,
    timestamps: false,
  }
);


  module.exports = Location;