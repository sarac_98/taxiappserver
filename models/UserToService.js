const Sequelize = require('sequelize');

const db = require('../database/database');
const User = require('./User');
const Service = require('./Service');
const RequestStatus = require('./RequestStatus');
const UserToService = db.define(
  'map_users_to_services',
  {
    service_id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    user_id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    start_adress: {
      type: Sequelize.STRING,
    },
    end_adress: {
      type: Sequelize.STRING
    }


  },
  {
    underscored: true,
    timestamps: false,
  }
);

UserToService.belongsTo(Service, { foreignKey: 'service_id', as: 'service' });
UserToService.belongsTo(User, { foreignKey: "user_id", as: 'user' });
UserToService.belongsTo(RequestStatus, { foreignKey: "request_status_id", as: 'requestStatus' });
User.belongsToMany(Service, { through: UserToService, foreignKey: 'user_id' })
Service.belongsToMany(User, { through: UserToService, foreignKey: 'service_id' })
module.exports = UserToService;
