const express = require('express');
const authController = require('../controllers/authController');
const authMiddlware = require('../middleware/authMiddleware');
const userController = require('../controllers/userController');
const factory = require('../controllers/handlerFactory');
const User = require('../models/User');

const router = express.Router();
router.route('/upload')
  .post(userController.upload.single('attachment'), userController.uploadFile);
router.post('/login', authController.login);
router.post('/signup', authController.signup);
router.post('/forgotPassword', authController.forgotPassword);
router.patch('/resetPassword/:token', authController.resetPassword);
router.post('/password/:userId', authController.changePassword);
router.get('/checkIfEmailExists', userController.checkIfEmailExists);

// router.use(authMiddlware.protect);
router
  .route('/')
  .get(userController.getAll)

router
  .route('/:id')
  .get(userController.getOne)
  .patch(userController.updateOne)
  .delete(userController.deleteOne);

module.exports = router;






module.exports = router;
