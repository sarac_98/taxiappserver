const express = require('express');
const requestStatusController = require('../controllers/requestStatusController');


const router = express.Router();

router
  .route('/')
  .get(requestStatusController.getAll)
router
  .route('/:id')
  .get(requestStatusController.getOne)
  .patch(requestStatusController.updateOne)
  .delete(requestStatusController.deleteOne);

module.exports = router;