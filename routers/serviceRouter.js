const express = require('express');

const serviceController = require('../controllers/serviceController');
const factory = require('../controllers/handlerFactory');
const Service = require('../models/Service');

const router = express.Router();
router
  .route('/')
  .get(serviceController.getAll)
  .post(serviceController.create)

router
  .route('/:id')
  .get(serviceController.getOne)
 
  .patch(serviceController.updateOne)
  .delete(serviceController.deleteOne);

module.exports = router;
