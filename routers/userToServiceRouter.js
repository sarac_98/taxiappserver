const express = require('express');
const userToServiceController = require('./../controllers/userToServiceController');

const router = express.Router();

router
  .route('/usersAssignedToService/:serviceId')
  .get(userToServiceController.usersAssignedToService)

router
  .route('/getUsersServices/:userId/:requestStatus')
  .get(userToServiceController.getUsersServices)

router
  .route('/updateRequestStatus/:serviceId/:userId/:requestId')
  .patch(userToServiceController.updateRequestStatus)

router
  .route('/createPdfUserServices')
  .get(userToServiceController.createUserServicePdf)
router
  .route('/')
  .get(userToServiceController.getAll)
  .post(userToServiceController.create);

// router
//   .route('/clients/:clientID')
//   .get(userToServiceController.getAllByClient);



// router
//   .route('/:id')
//   .get(userToServiceController.getOne)
//   .patch(userToServiceController.updateOne)
//   .delete(userToServiceController.deleteOne);

module.exports = router;