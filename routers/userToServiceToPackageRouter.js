const express = require('express');
const userToServiceToPackageController = require('./../controllers/userToServiceToPackageController');

const router = express.Router();

router
  .route('/')
  .get(userToServiceToPackageController.getAll)
  .post(userToServiceToPackageController.create);

// router
//   .route('/clients/:clientID')
//   .get(userToServiceController.getAllByClient);

// router.delete('/removeByServiceAndClient', serviceToClientController.deleteByClientServiceId)

router
  .route('/:id')
  .get(userToServiceToPackageController.getOne)
  .patch(userToServiceToPackageController.updateOne)
  .delete(userToServiceToPackageController.deleteOne);

module.exports = router;