const express = require('express');

const locationController = require('../controllers/locationController');
const factory = require('../controllers/handlerFactory');
const Location = require('../models/Location');

const router = express.Router();
router
  .route('/checkIfLocationExists')
  .get(locationController.checkIfLocationExists)
  
router
  .route('/')
  .get(locationController.getAll)
  .post(locationController.create)

router
  .route('/:id')
  .get(locationController.getOne)
  .patch(locationController.updateOne)
  .delete(locationController.deleteOne);

module.exports = router;