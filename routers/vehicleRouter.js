const express = require('express');

const vehicleController = require('../controllers/vehicleController');
const factory = require('../controllers/handlerFactory');
const Vehicle = require('../models/Vehicle');

const router = express.Router();
router
  .route('/')
  .get(vehicleController.getAll)

router
  .route('/:id')
  .get(vehicleController.getOne)
  .patch(vehicleController.updateOne)
  .delete(vehicleController.deleteOne);

module.exports = router;