const express = require('express');

const packageController = require('../controllers/packageController');
const factory = require('../controllers/handlerFactory');
const Package = require('../models/Package');

const router = express.Router();

router
  .route('/')
  .get(packageController.getAll)
  

router
  .route('/:id')
  .get(packageController.getOne)
  .patch(packageController.updateOne)
  .delete(packageController.deleteOne);

module.exports = router;