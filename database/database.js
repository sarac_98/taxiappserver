const Sequelize = require('sequelize');
const dotenv = require('dotenv');
require('dotenv').config()
dotenv.config({ path: __dirname + '../config.env' });

module.exports = new Sequelize(
  'taxiapp',
  'root',
  'root',
  {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: 'false',

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
);
