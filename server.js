const app = require('./app');
const db = require('./database/database');


const dotenv = require('dotenv');
process.setMaxListeners(0);

require('dotenv').config()
const port = 4000;
const server = app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});

db.authenticate()
  .then(() => {
    console.log('Database connected...');
  })
  .catch(err => console.log('Error: ' + err));

db.sync().then(() => {});

process.on('unhandledRejection', err => {
  console.log('UNHANDLED REJECTION! 💥 Shutting down...');
  console.log(err.name, err.message);
  server.close(() => {
    process.exit(1);
  });
});

process.on('SIGTERM', () => {
  console.log('👋 SIGTERM RECEIVED. Shutting down gracefully');
  server.close(() => {
    console.log('💥 Process terminated!');
  });
});