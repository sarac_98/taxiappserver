module.exports = {
    USER_TABLE_NAME: 'users',
  
    //USER MODEL FIELDS
     USER_ID: 'user_id',
     USER_FULL_NAME: 'full_name',
     USER_USERNAME: 'username',
     USER_ADRESS: 'adress',
     USER_PHONE_NUMBER: 'phone_number',
     USER_ONLINE_STATUS: 'online_status',
     USER_USER_TYPE: 'user_type',

  
    //FOREIGN KEYS
    
    USER_FOREIGN_KEY_SERVICE: 'service_id',
    
  
    //ASSOCIATION MODEL NAMES
    MODEL_SERVICE_NAME: 'service'
    
  
    //ASSOCIATION MODEL FIELDS
    // CONSTANT_ID: 'id',
  
  
  };