module.exports = {
    SERVICE_TABLE_NAME: 'service',
  
    //SERVICES MODEL FIELDS
     SERVICE_ID: 'id',
     SERVICE_DATE: 'date',
     SERVICE_TIME: 'time',
  
    //FOREIGN KEYS
    
  
    //ASSOCIATION MODEL NAMES
    MODEL_LOCATION_NAME: 'location',
    MODEL_VEHICLE_NAME: 'vehicle',
    
  
    //ASSOCIATION MODEL FIELDS
    LOCATION_ID : 'id',
    CITY: 'city',
    VEHICLE_ID: 'id',
    VEHICLE_MODEL:'model',
    SEATS_NUM: 'seats_num'
    
  };