const express = require('express');
const userRouter = require('./routers/userRouter');
const serviceRouter = require('./routers/serviceRouter');
const packageRouter = require('./routers/packageRouter');
const locationRouter = require('./routers/locationRouter');
const userToServiceRouter = require('./routers/userToServiceRouter');
const vehicleRouter = require('./routers/vehicleRouter')
const userToServiceToPackageRouter = require('./routers/userToServiceToPackageRouter');
const path = require('path');

const requestStatusRouter = require('./routers/requestStatusRouter')
const bodyParser = require('body-parser');
const fs = require("fs");

var cors = require('cors')

const app = express();
//app.use(express.static('ftp'));
app.use( '/ftp', express.static( path.join(__dirname, "ftp") ));
app.use(cors())
app.use(bodyParser.json());
app.use('/users', (req, res, next) => { console.log("Request trlalala", req.body, req.params, req.query, req.originalUrl); next(); }, userRouter);
app.use('/services', serviceRouter)
app.use('/package', packageRouter)
app.use('/usersToServices', userToServiceRouter)
app.use('/userToServiceToPackage', userToServiceToPackageRouter);
app.use('/locations', locationRouter)
app.use('/requestStatus', requestStatusRouter)
app.use('/vehicles', vehicleRouter)
app.get("/pdf", (req, res) => {
    var file = fs.createReadStream("./ftp/VožnjeZaDanas.pdf");
    file.pipe(res);
});
module.exports = app;